import styled from 'styled-components'

const defaultIndent = '20px';

export const EasterEggContainer = styled.div`
  position: fixed; 
  top: 0; 
  left: 0;
  backdrop-filter: blur(3px);
  background: rgba(0, 0, 0, .3);
  width: 100%;
  height: 100%;
  z-index: 9;
  overflow: hidden; 
  display: flex; 
  align-items: center;
  justify-content: center;
    
  .easter-egg-block {
    position: absolute; 
    left: ${defaultIndent};
    top: ${defaultIndent}; 
    background: rgba(0, 0, 0, .6);
    font-size: 24px; 
    text-transform: uppercase;
    padding: 10px;
    font-weight: bold; 
    color: white;
  }
  
  .easter-egg-video {
    width: 80%;
    height: 80%;
    background: black;
  }
  
  .easter-egg-close {
    position: absolute;
    cursor: pointer;
    right: ${defaultIndent};
    top: ${defaultIndent};
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background: white;
    display: flex;
    align-items: center;
    justify-content: center;
    box-shadow: 0 3px 10px rgb(0 0 0 / 50%);
    transition: .3s ease-in-out;
    
    &:hover {
      transform: scale(1.25);
    }
  }
  
  .easter-egg-image {
    max-width: 80%;
  }
  
  .easter-egg-yecgaa {
    position: absolute;
    width: 150px;
    height: 300px; 
    left: -150px;
    top: 10%;
    background: url('/images/cj.png') center no-repeat;
    background-size: contain;
    animation: cj 3s;
  }
  
  @keyframes cj {
    0% { 
      left: -150px; 
      top: 10%;
      rotate(10deg)
    }
    40% { 
      left: 40%;
      top: 60%; 
      transform: rotate(-45deg);
    }
    100% { 
      left: 110%;
      top: 30%; 
      transform: scale(1.8) rotate(60deg);
    }
  }
`;