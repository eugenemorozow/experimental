import React, {useEffect, useState, useRef} from 'react';
import {EasterEggContainer} from './easterEgg.style';
import ReactPlayer from 'react-player'

export default ({timeToShow}) => {
  // Easter egg
  const defaultTimeout = timeToShow || 3000;

  const [easterEggTitle, setEasterEggTitle] = useState(null);
  const [easterEggClassName, setEasterEggClassName] = useState(null);

  const [isVisible, setIsVisible] = useState(false);
  const [showTitle, setShowTitle] = useState(true);

  const [html, setHtml] = useState(null);
  const [music, setMusic] = useState(null);
  const [image, setImage] = useState(null);
  const [video, setVideo] = useState(null);

  const [timing, setTiming] = useState(defaultTimeout);
  const item = 'text';

  const text = () => {
    return localStorage.getItem(item);
  }

  function setText(value) {
    localStorage.setItem(item, value);
  }

  function getCheatIndex(cheats, str) {
    let currentIndex = null;
    cheats.forEach((cheat, index) => {
      if (str === cheat.title.substring(0, str.length)) {
        currentIndex = index;
        return true
      }
    });
    return currentIndex
  }

  useEffect(() => {
    const handleGlobalKeyDown = (e) => {
      let cheats = [
        {
          title: 'yecgaa',
          html: null,
          addClass: true,
          timing: 3000,
          video: null,
          music: null,
          image: null
        },
        {
          title: 'pascal',
          html: null,
          addClass: false,
          timing: 8000,
          video: null,
          music: '/music/mission.mp3',
          image: '/images/complete.png'
        },
        {
          title: 'bass',
          html: null,
          addClass: false,
          timing: 8000,
          video: null,
          music: '/music/mission_bass.mp3',
          image: '/images/complete.png'
        },
        {
          title: 'react',
          html: '<div class="glitch">Govno</div>',
          addClass: false,
          timing: null,
          video: null,
          music: null,
          image: null
        },
        {
          title: 'nigger',
          html: null,
          addClass: false,
          timing: 0,
          video: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
          music: null,
          image: null
        },
      ];
      let currentText = text() + e.key;
      setText(currentText);
      let index = getCheatIndex(cheats, currentText);
      if (index !== null) {
        if (currentText === cheats[index].title.substring(0, currentText.length)) {
          if (currentText === cheats[index].title) {
            let cheatCode = cheats[index];
            setTiming((cheatCode.timing === 0) ? null : (cheatCode.timing) ? cheatCode.timing : defaultTimeout);
            setEasterEggTitle(cheatCode.title);
            setEasterEggClassName((cheatCode.addClass) ? `easter-egg-${cheatCode.title}` : null);
            setVideo(cheatCode.video || null);
            setHtml(cheatCode.html || null);
            setMusic(cheatCode.music || null);
            setImage(cheatCode.image || null);
            setIsVisible(true);
            setShowTitle(true);
            setText('');
          }
          return true
        }
      }
      setText('');
    };

    document.addEventListener('keydown', handleGlobalKeyDown);
    return () => {
      document.removeEventListener('keydown', handleGlobalKeyDown);
    };
  }, []);

  useEffect(() => {
    if (isVisible && (timing !== null)) {
      let visibleTimer = setTimeout(() => {
        setIsVisible(false);
      }, timing);
      return () => {
        clearTimeout(visibleTimer)
      }
    }
  }, [isVisible]);

  useEffect(() => {
    if (showTitle) {
      let titleTimer = setTimeout(() => {
        setShowTitle(false);
      }, (timing && (timing < 2000)) ? timing : 2000);
      return () => {
        clearTimeout(titleTimer)
      }
    }
  }, [showTitle]);
  // Easter egg end

  return (
    <>
      {isVisible ?
        <EasterEggContainer>
          <iframe src="/music/activate.mp3" allow="autoplay" style={{display: 'none'}} id="iframeAudio"/>
          {
            showTitle && (
              <div className="easter-egg-block">Чит-код {easterEggTitle} активирован</div>
            )
          }
          {
            (timing === null) && (
              <div className="easter-egg-close" onClick={() => {
                setIsVisible(false)
              }}><img alt="close" src='/images/close.svg' width="18px"/></div>
            )
          }
          {
            easterEggClassName && (
              <div className={easterEggClassName}/>
            )
          }
          {
            html && (
              <div dangerouslySetInnerHTML={{__html: html}}/>
            )
          }
          {
            music && (
              <iframe src={music} allow="autoplay" style={{display: 'none'}} id="music"/>
            )
          }
          {
            image && (
              <img src={image} className="easter-egg-image"/>
            )
          }
          {
            video && (
              <div className="easter-egg-video">
                <ReactPlayer url={video} playing={true} controls={false} width="100%" height="100%"/>
              </div>
            )
          }
        </EasterEggContainer>
        :
        null
      }
    </>
  )
}