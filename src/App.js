import EasterEgg from "./components/EasterEgg";

function App() {
  return (
    <div className="App">
      <EasterEgg timeToShow={3000}/>
    </div>
  );
}

export default App;
